﻿using PRN221_Lab1.Models;
using PRN221_Lab1.Services;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Assignment
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ProductServices _productServices;
        CategoryServices _categoryServices;
        public MainWindow()
        {
            InitializeComponent();
            _categoryServices = new CategoryServices();
            _productServices = new ProductServices();
            cbCategory.ItemsSource = _categoryServices.GetCategories();
            cbCategory.SelectedIndex = 0;
            txtCateId.ItemsSource = _categoryServices.GetCategories();

            txtCateId.SelectedValuePath = "CategoryId";
            txtCateId.DisplayMemberPath = "CategoryName";
        }

        private void cbCategories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadData();
        }

        private void lbProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ProductVM productVM = (ProductVM)lbProducts.SelectedItem;
            if (productVM != null)
            {

                txtId.Text = productVM.ProductId.ToString();
                txtName.Text = productVM.ProductName;
                txtCateId.SelectedValue = productVM.CategoryName;
                txtPrice.Text = productVM.UnitPrice.ToString();
                txtStock.Text = productVM.UnitsInStock.ToString();

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ProductVM pvm = ReciveData();
            if (pvm != null)
            {
                if (pvm.UnitPrice == null)
                {
                    MessageBox.Show("Invalid Unit Price format. Please enter a valid decimal number.");
                    // Return without updating if the unit price format is invalid
                    return;
                }

                _productServices = new ProductServices();
                _productServices.Update(pvm);
                LoadData();
            }
        }
        private void LoadData()
        {
            int CategoryId = ((Category)cbCategory.SelectedItem).CategoryId;
            lbProducts.ItemsSource = _productServices.GetProducts(CategoryId);
            ResetData();
        }
        private void ResetData()
        {
            txtId.Text = "";
            txtName.Text = "";
            txtCateId.Text = "";
            txtPrice.Text = "";
            txtStock.Text = "";
        }
        private ProductVM ReciveData()
        {
            int Id;
            if (!int.TryParse(txtId.Text, out Id))
            {
                MessageBox.Show("Invalid Product ID. Please enter a valid integer.");
                return null;
            }

            string name = txtName.Text;


            string namec = txtCateId.Text;
            var category = _categoryServices.GetCategories().FirstOrDefault(s => s.CategoryName.Equals(namec));
            if (category == null)
            {
                MessageBox.Show("Invalid Category Name. Please select a valid category.");
                return null;
            }
            int idc = category.CategoryId;

            decimal price;
            if (!decimal.TryParse(txtPrice.Text, out price))
            {
                MessageBox.Show("Invalid Unit Price. Please enter a valid decimal number.");
                return null;
            }

            short stock;
            if (!short.TryParse(txtStock.Text, out stock))
            {
                MessageBox.Show("Invalid Units In Stock. Please enter a valid short integer.");
                return null;
            }


            ProductVM productVM = new ProductVM()
            {
                ProductId = Id,
                ProductName = name,
                UnitPrice = price,
                UnitsInStock = stock,
                CategoryId = idc
            };
            return productVM;
        }

        private ProductVM ReciveData1()
        {
            string name = txtName.Text;
            string namec = txtCateId.Text;
            var category = _categoryServices.GetCategories().FirstOrDefault(s => s.CategoryName.Equals(namec));
            if (category == null)
            {
                MessageBox.Show("Invalid Category Name. Please select a valid category.");
                return null;
            }
            int idc = category.CategoryId;

            decimal price;
            if (!decimal.TryParse(txtPrice.Text, out price))
            {
                MessageBox.Show("Invalid Unit Price. Please enter a valid decimal number.");
                return null;
            }

            short stock;
            if (!short.TryParse(txtStock.Text, out stock))
            {
                MessageBox.Show("Invalid Units In Stock. Please enter a valid short integer.");
                return null;
            }

            ProductVM productVM = new ProductVM()
            {

                ProductName = name,
                UnitPrice = price,
                UnitsInStock = stock,
                CategoryId = idc
            };
            return productVM;
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            ProductVM vm = ReciveData();
            _productServices = new ProductServices();
            _productServices.Delete(vm);
            LoadData();

        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            ProductVM vm = ReciveData1();
            _productServices = new ProductServices();
            _productServices.Add(vm);
            LoadData();

        }

        private void revert_Click(object sender, RoutedEventArgs e)
        {
            BieuDO bieu = new BieuDO();
            this.Close();
            bieu.Show();

        }
    }
}